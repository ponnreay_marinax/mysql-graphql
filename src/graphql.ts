
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export class BoughtHistory {
    id: string;
    product_id: string;
    user_id: string;
    date: DateTime;
    product: Product;
    user: User;
}

export abstract class IMutation {
    abstract bought_history(product_id: string, user_id: string, date: string): BoughtHistory | Promise<BoughtHistory>;

    abstract product(name: string, price: number): Product | Promise<Product>;

    abstract user(name: string, age: number, address: string): User | Promise<User>;
}

export class Product {
    id: string;
    name: string;
    price: number;
    bought_histories?: BoughtHistory[];
}

export abstract class IQuery {
    abstract bought_histories(): BoughtHistory[] | Promise<BoughtHistory[]>;

    abstract products(): Product[] | Promise<Product[]>;

    abstract product(name: string): Product | Promise<Product>;

    abstract users(page?: number, limit?: number): User[] | Promise<User[]>;
}

export class User {
    id: string;
    name: string;
    age: number;
    address: string;
    bought_histories?: BoughtHistory[];
}

export type DateTime = any;

import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from "typeorm";
import { UserEntity } from "../user/user.entity";
import { ProductEntity } from "../product/product.entity";


@Entity('bought_histories')
export class BoughtHistoryEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({type: 'varchar', length: 255})
    product_id: string

    @Column({type: 'varchar', length: 255})
    user_id: string

    @Column('datetime')
    date: string

    @ManyToOne(type => UserEntity, user => user.bought_histories)
    @JoinColumn({
        name: 'user_id'
    })
    user: UserEntity

    @ManyToOne(type => ProductEntity, product => product.bought_histories)
    @JoinColumn({
        name: 'product_id'
    })
    product: ProductEntity
}
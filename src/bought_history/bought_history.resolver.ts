import { Resolver, Query, Mutation, Args } from "@nestjs/graphql";
import { BoughtHistoryService } from "./bought_history.service";


@Resolver('BoughtHistory')
export class BoughtHistoryResolver {

    constructor(
        private boughtHistoryService: BoughtHistoryService
    ) {}

    @Query()
    bought_histories() {
        return this.boughtHistoryService.all()
    }

    @Mutation()
    bought_history(@Args() { product_id, user_id, date}) {
        return this.boughtHistoryService.create({product_id, user_id, date})
    }
}


import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BoughtHistoryEntity } from "./bought_history.entity";
import { BoughtHistoryResolver } from "./bought_history.resolver";
import { BoughtHistoryService } from "./bought_history.service";


@Module({
    imports: [ TypeOrmModule.forFeature([BoughtHistoryEntity])],
    providers: [BoughtHistoryResolver, BoughtHistoryService],
})
export class BoughtHistoryModule {}
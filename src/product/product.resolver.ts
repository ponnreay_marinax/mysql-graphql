import { Resolver, Query, Mutation, Args } from "@nestjs/graphql";
import { ProductService } from "./product.service";

@Resolver('Product')
export class ProductResolver {

    constructor(
        private productService: ProductService
    ) { }

    @Query()
    products() {
        return this.productService.all()
    }

    @Mutation()
    product(@Args() { name, price }) {
        return this.productService.create({name, price})
    }
}
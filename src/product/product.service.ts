import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { ProductEntity } from "./product.entity";
import { Repository } from "typeorm";


@Injectable()
export class ProductService {

    constructor(
        @InjectRepository(ProductEntity) private productRepo: Repository<ProductEntity>
    ) {}

    all() {
        return this.productRepo.find({
            relations: ['bought_histories', 'bought_histories.user']
        })
    }

    create(data: Partial<ProductEntity>) {
        return this.productRepo.save(this.productRepo.create(data))
    }
}
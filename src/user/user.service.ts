import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { Repository } from 'typeorm';
import * as moment from 'moment-timezone'

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>
    ) {}

    async all(query: {page: number, limit: number}) {
        const take = query.limit || 100000
        const skip = (query.page ? query.page - 1: 0) * take
        return await this.userRepo.find({
            relations: ['bought_histories', 'bought_histories.product'],
            take, skip,
        })
    }

    async create(data: Partial<UserEntity>) {
        const entity = this.userRepo.create(data)
        return this.userRepo.save(entity)
    }
}

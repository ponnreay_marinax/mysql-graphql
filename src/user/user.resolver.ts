import { Resolver, Query, Mutation, Args } from "@nestjs/graphql";
import { UserService } from "./user.service";


@Resolver('User')
export class UserResolver {

    constructor(
        private userService: UserService
    ) {}

    @Query()
    users(@Args() {page, limit}) {
        return this.userService.all({page, limit})
    }

    @Mutation()
    user(@Args() {name, age, address}) {
        return this.userService.create({name, age, address})
    }
}
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn } from 'typeorm'
import { BoughtHistoryEntity } from '../bought_history/bought_history.entity'

@Entity('users')
export class UserEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({
        type: 'varchar',
        length: 255
    })
    name: string

    @Column({type: 'smallint'})
    age: number

    @Column({
        type: 'varchar',
        length: 255
    })
    address: string

    @OneToMany(type => BoughtHistoryEntity, bought_history => bought_history.user)
    @JoinColumn({
        name: 'id'
    })
    bought_histories: BoughtHistoryEntity[]
}
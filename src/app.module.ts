import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql'
import { join } from 'path';
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserModule } from './user/user.module';
import { ProductModule } from './product/product.module';
import { BoughtHistoryModule } from './bought_history/bought_history.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './shared/logging.interceptor';

@Module({
    imports: [
        GraphQLModule.forRoot({
            debug: true,
            playground: true,
            typePaths: ['./**/*.graphql'],
            // definitions: {
            //     path: join(process.cwd(), 'src/graphql.ts'),
            //     outputAs: 'class',
            // },
        }),
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'test',
            password: '12345',
            database: 'test',
            logging: false,
            entities: ['./**/*.entity.js'],
            synchronize: false,
        }),
        UserModule,
        ProductModule,
        BoughtHistoryModule,
    ],
    controllers: [],
    providers: [{
        provide: APP_INTERCEPTOR,
        useClass: LoggingInterceptor
    }],
})
export class AppModule { }
